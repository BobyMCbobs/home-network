( 
HOST=192.168.1.4
talosctl \
    -n "${HOST}" \
    -e "${HOST}" \
    etcd snapshot \
    ../home-network-extended/etcd-backups/"$(date +%Y-%m-%d-%H-%M).db"
 ) 2>&1 ; :
